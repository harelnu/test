#!/bin/bash

if [ ! -x "python/get_ports.py" ]
then
    chmod +x python/clients.py
    echo here
fi

python_file="python/clients.py"
len=$(python/get_ports.py)

if [ ! -x $python_file ]
then
    chmod +x python/clients.py
    echo here
fi

i=0
while [ $i -lt $len ]
do
    port=$(python/get_ports.py $i)
    iperf3 -s -p $port &
    let 'i++'
done

python/clients.py
sleep 25
kill -9 $(ps -aux | grep iperf3 | awk '{print $2}')
exit
