#!/usr/bin/python3
import iperf3
import multiprocessing as mp
import os
import json
import time

RESULT_JSON_PATH = os.getcwd() + r"/json/results.json"
INPUT_JSON_PATH = os.getcwd() + r"/json/input.json"
JSON_DIR = os.getcwd() + r"/json/"


def build_file_name() -> str:
    curr_time = time.time() * 1000
    name = "result" + str(curr_time) + ".json"
    path = JSON_DIR + name
    file = open(path, 'x')
    file.close()
    return path


def start_client(ip, path, port=5201, reverse=False, conn_count=1):
    client = iperf3.Client()
    client.server_hostname = ip
    client.port = port
    client.json_output = True
    client.num_streams = conn_count
    client.reverse = reverse
    result = client.run().json

    with open(path, 'r+') as result_json_file:
        if os.stat(path).st_size == 0:
            list = [result]
            json.dump(list, result_json_file, indent=4)
        else:
            json_list = json.load(result_json_file)
            json_list.append(result)
            result_json_file.seek(0)
            json.dump(json_list, result_json_file, indent=4)


def run_clients(ip, port, conn_count):
    path = build_file_name()
    start_client(ip, path, int(port), conn_count=int(conn_count))
    start_client(ip, path, int(port), True, int(conn_count))


def main():
    var_list = []
    temp_list=[]
    procs = []

    with open(INPUT_JSON_PATH, 'r') as raw:
        json_object = json.load(raw)

    for i in range(len(json_object["server"])):
        temp_list = json_object["server"][i]
        temp_list.append(json_object["connections_count"][i])
        var_list.append(temp_list)
    
    for c_list in var_list:
        proc = mp.Process(target=run_clients, args=(c_list[0], c_list[1], c_list[2]))
        procs.append(proc)
        proc.start()
    
    for proc in procs:
        proc.join()


if __name__ == '__main__':
    main()
