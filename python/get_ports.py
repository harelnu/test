#!/usr/bin/python3
import json
import os
import sys


INPUT_JSON_PATH = os.getcwd() + r"/json/input.json"


def main():
    ports = []

    with open(INPUT_JSON_PATH, 'r') as raw:
        json_object = json.load(raw)

    for list in json_object["server"]:
        ports.append(list[1])

    if len(sys.argv) == 1:
        print(len(ports))
    else:
        print(ports[int(sys.argv[1])])


if __name__ == '__main__':
    main()